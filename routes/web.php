<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('manage-regions', 'RegionsController@manage');
Route::get('list-regions', 'RegionsController@list');
Route::resource('regions','RegionsController');

Route::get('manage-products', 'ProductsController@manage');
Route::get('list-products', 'ProductsController@list');
Route::resource('products','ProductsController');

Route::get('manage-prices', 'PricesController@manage');
Route::get('price-import', 'PricesController@get_import');
Route::post('price-import', 'PricesController@post_import');
Route::post('price-import', 'PricesController@post_import');
Route::post('price-remove-all', 'PricesController@remove_all');
Route::resource('prices','PricesController');

Route::get('manage-discounts', 'DiscountsController@manage');
Route::resource('discounts','DiscountsController');

Route::get('price-calculator', 'CalculationsController@index');
Route::get('calculate-price', 'CalculationsController@calculate');
