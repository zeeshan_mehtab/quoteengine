<?php

namespace App\Http\Requests\Import;

use Maatwebsite\Excel\Files\ExcelFile ;
use Illuminate\Support\Facades\Input;

class PriceImport extends ExcelFile {

    public function getFile()
    {
        // Import a user provided file
        $file = Input::file('file');
        return $file->getPathname();
    }

    public function getFilters()
    {
        return [
            
        ];
    }

}
