<?php

namespace App\Http\Requests\Import;

use Maatwebsite\Excel\Files\ImportHandler;
use Illuminate\Support\Facades\DB;

class PriceImportHandler implements ImportHandler
{

    public function handle($file)
    {

        $book = $file->get();
        $logs = [];

        $productsMap = $this->getProductsMap();
        $regionsMap = $this->getRegionsMap();

        foreach ($book as $sheet) {

            $product = $sheet->getTitle();
            if (!array_key_exists($product, $productsMap)) {
                $logs[] = "Error: Could not find product: {$product}";
                continue;
            }

            $productId = $productsMap[$product];
            $logs[] = "Importing Sheet: {$sheet->getTitle()}";

            foreach ($sheet as $row) {


                $ageRange = explode("-", trim($row->Edades));
                if (sizeof($ageRange) == 1) {
                    $fromAge = $toAge = trim($ageRange[0]);
                } else {
                    $fromAge = trim($ageRange[0]);
                    $toAge = trim($ageRange[1]);
                }

                if (empty($fromAge) || empty($toAge) || is_nan($fromAge) || is_nan($toAge)) {
                    $logs[] = "---Invalid date range provided";
                }

                $fromAge = $fromAge < 0 ? 0 : $fromAge;
                $toAge = $toAge > 100 ? 100 : $toAge;

                for ($age = $fromAge; $age <= $toAge; $age++) {

                    foreach ($row as $key => $value) {

                        if ($key == "Edades" || $key == "Ages") {
                            continue;
                        }

                        if (empty($value)) {
                            continue;
                        }

                        if ($key == "Resto" || $key == "All") {
                            $this->createPrice($productId, null, $age, $value);
                        } else {
                            $regions = explode(",", $key);
                            foreach ($regions as $region) {
                                $region = trim($region);
                                if (!array_key_exists($region, $regionsMap)) {
                                    $logs[] = "Error: Could not find region: {$region}";
                                    continue;
                                }
                                $regionId = $regionsMap[$region];
                                $this->createPrice($productId, $regionId, $age, $value);
                            }
                        }
                    }
                }
            }
        }

        echo "<pre>";
        print_r($logs);
        exit;

    }

    private function createPrice($productId, $regionId, $age, $price)
    {
        if (empty($price)) {
            return;
        }

        $priceRow = DB::table('prices')
            ->where('product_id', $productId)
            ->where('region_id', $regionId)
            ->where('age', $age)
            ->where('price', $price)
            ->first();

        if ($priceRow) {
            DB::table('prices')
                ->where('id', $priceRow->id)
                ->update(['price' => $price]);
        } else {
            DB::table('prices')->insert([
                'product_id' => $productId,
                'region_id' => $regionId,
                'age' => $age,
                'price' => $price,
            ]);
        }
    }

    private function getRegionsMap()
    {
        $regions = DB::table('regions')->get();
        $map = [];
        foreach ($regions as $region) {
            $map[$region->name] = $region->id;
        }
        return $map;
    }

    private function getProductsMap()
    {
        $products = DB::table('products')->get();
        $map = [];
        foreach ($products as $product) {
            $map[$product->name] = $product->id;
        }
        return $map;
    }

    private function clean_number($number)
    {
        return intval(preg_replace('/[^\d.]/', '', $number));
    }

}
