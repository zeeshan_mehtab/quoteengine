<?php

namespace App\Http\Controllers;

use App\Http\Requests\Import\PriceImport;
use Illuminate\Http\Request;
use App\Models\Price;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class PricesController extends Controller
{

    public function manage()
    {
        return view('admin.prices.manage-vue');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productId = $request->input('product');
        $regionId = $request->input('region');

        $prices = Price::with('product', 'region');
        if (!empty($productId)) {
            $prices = $prices->where('product_id', $productId);
        }

        if (!empty($regionId)) {
            $prices = $prices->where('region_id', $regionId);
        }

        $prices = $prices->orderBy('product_id')
                        ->orderBy('region_id')
                        ->orderBy('age')
                        ->paginate(20);

        $response = [
            'pagination' => [
                'total' => $prices->total(),
                'per_page' => $prices->perPage(),
                'current_page' => $prices->currentPage(),
                'last_page' => $prices->lastPage(),
                'from' => $prices->firstItem(),
                'to' => $prices->lastItem()
            ],
            'data' => $prices
        ];

        return response()->json($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'age' => 'required',
            'price' => 'required',
            'product_id' => 'required',
            'region_id' => 'required',
        ]);

        $create = Price::create($request->all());

        return response()->json($create);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'age' => 'required',
            'price' => 'required',
            'product_id' => 'required',
            'region_id' => 'required',
        ]);

        $edit = Price::find($id)->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Price::find($id)->delete();
        return response()->json(['done']);
    }

    public function get_import()
    {
        return view("admin.prices.import");
    }

    public function post_import(PriceImport $import)
    {

        $import->handleImport();

        return redirect("manage-prices")->with(['success' => 'File imported successfully.']);
    }

    public function remove_all()
    {
        DB::table('prices')->truncate();
        return redirect("manage-prices")->with(['success' => 'All prices removed successfully.']);
    }

}
