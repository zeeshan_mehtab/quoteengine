<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Price;
use App\Models\Discount;

class CalculationsController extends Controller
{

    public function index()
    {
        return view('admin.calculations.index');
    }

    /**
     * calculate.
     *
     * @return \Illuminate\Http\Response
     */
    public function calculate(Request $request)
    {
        
        $productId = $request->input('product_id');    
        $regionId = $request->input('region_id');
        $cycleMonths = $request->input('payment_cycle');
        $ages = explode(",", $request->input('ages'));
        
        $total = $this->getTotal($productId, $regionId, $cycleMonths, $ages);

        return response()->json($total);
    }

    private function getTotal($productId, $regionId, $cycleMonths, $ages) {
        
        $no_of_person = sizeof($ages);
        $total = 0;
        $logs = [];

        foreach ($ages as $key => $value) {
            $price = Price::where('product_id', $productId)
                          ->where('region_id', $regionId)
                          ->where('age', $value)
                          ->first();

            if(!$price) {
                $price = Price::where('product_id', $productId)
                    ->whereNull('region_id')
                    ->where('age', $value)
                    ->first();
            }

            if($price) {
                if($no_of_person > 1) {
                    //reduce the surcharge
                    $originalPrice = $price->price;
                    $wihtoutSurcharge = round(((100 * $originalPrice) / (100 + 8)), 2);
                    $total += $wihtoutSurcharge;
                    $logs[] = "No of person is more than 1 so reducing 8% surcharge price for Product:{$productId}, Region:{$regionId}, Age:{$value}";
                } else {
                    $total += $price->price;
                }    
            } else {
                return 0;       
            }           
        }

        // Get discount based on number of persons
        $discount = Discount::where('product_id', $productId)
                            ->where('no_of_person', '<=', $no_of_person)
                            ->where('payment_cycle', 0)
                            ->orderBy('no_of_person', 'desc')
                            ->first();

        if($discount && $discount->discount > 0) {
            $discountValue = ($total / 100) * $discount->discount;
            $total -= $discountValue;
            $logs[] = "Discount found for Product:{$productId} and Min. No of Person:{$no_of_person}";
        }

        // Get discount based on payment cycle
        $discount = Discount::where('product_id', $productId)
                            ->where('no_of_person', '<=', $no_of_person)
                            ->where('payment_cycle', $cycleMonths)
                            ->orderBy('no_of_person', 'desc')
                            ->first();            

        if($discount && $discount->discount > 0) {
            $discountValue = ($total / 100) * $discount->discount;
            $total -= $discountValue;
            $logs[] = "Discount found for Product:{$productId} and Min. No of Person:{$no_of_person} and Payment Cycle Months:{$cycleMonths}";
        }

        return [
            "total" => $total,
            "logs" => $logs,
        ];
    }
}
