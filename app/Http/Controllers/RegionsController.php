<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Region;

class RegionsController extends Controller
{

    public function manage()
    {
        return view('admin.regions.manage-vue');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regions = Region::latest()->paginate(5);

        $response = [
            'pagination' => [
                'total' => $regions->total(),
                'per_page' => $regions->perPage(),
                'current_page' => $regions->currentPage(),
                'last_page' => $regions->lastPage(),
                'from' => $regions->firstItem(),
                'to' => $regions->lastItem()
            ],
            'data' => $regions
        ];

        return response()->json($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $create = Region::create($request->all());

        return response()->json($create);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $edit = Region::find($id)->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Region::find($id)->delete();
        return response()->json(['done']);
    }

    public function list() {
        $response = Region::orderBy('name', 'asc')->get();
        return response()->json($response);
    }
}
