<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Discount;

class DiscountsController extends Controller
{

    public function manage()
    {
        return view('admin.discounts.manage-vue');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $productId = $request->input('product');

        $discounts = Discount::with('product');
        if(!empty($productId)) {
            $discounts = $discounts->where('product_id', $productId);
        }

        $discounts = $discounts->latest()->paginate(10);
        

        $response = [
            'pagination' => [
                'total' => $discounts->total(),
                'per_page' => $discounts->perPage(),
                'current_page' => $discounts->currentPage(),
                'last_page' => $discounts->lastPage(),
                'from' => $discounts->firstItem(),
                'to' => $discounts->lastItem()
            ],
            'data' => $discounts
        ];

        return response()->json($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'no_of_person' => 'required|numeric|min:1',
            'discount' => 'required|numeric|min:1',
            'product_id' => 'required|numeric|min:1',
            'payment_cycle' => 'required|numeric|min:0',
        ]);

        $create = Discount::create($request->all());

        return response()->json($create);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'no_of_person' => 'required|numeric|min:1',
            'discount' => 'required|numeric|min:1',
            'product_id' => 'required|numeric|min:1',
            'payment_cycle' => 'required|numeric|min:0',
        ]);

        $edit = Discount::find($id)->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Discount::find($id)->delete();
        return response()->json(['done']);
    }

    public function list() {
        $response = Discount::orderBy('name', 'asc')->get();
        return response()->json($response);
    }

}
