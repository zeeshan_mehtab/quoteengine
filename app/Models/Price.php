<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public $fillable = ['age', 'price', 'product_id', 'region_id'];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    public function region() {
        return $this->belongsTo('App\Models\Region');
    }
}
