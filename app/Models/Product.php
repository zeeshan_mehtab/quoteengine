<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = ['name','description'];

    public function prices() {
        return $this->hasMany('App\Models\Price');
    }

    public function discounts() {
        return $this->hasMany('App\Models\Discount');
    }
}
