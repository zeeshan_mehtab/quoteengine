<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public $fillable = ['discount', 'no_of_person', 'payment_cycle', 'product_id'];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
