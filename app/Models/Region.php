<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $fillable = ['name','description'];

    public function prices() {
        return $this->hasMany('App\Models\Price');
    }
}
