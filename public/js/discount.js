Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({

  el: '#manage-vue',

  data: {
    discounts: [],
    pagination: {
        total: 0, 
        per_page: 2,
        from: 1, 
        to: 0,
        current_page: 1
      },
    offset: 4,
    formErrors:{},
    formErrorsUpdate:{},
    newDiscount : {'no_of_person':'','discount':'','product_id':'','payment_cycle':''},
    fillDiscount : {'no_of_person':'','discount':'','product_id':'','payment_cycle':'', 'id': ''},
    regions:{},
    products:{},
    selectedProduct:'',
    ageSplit: 1,
    searchProduct:'',
  },

  computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },

  ready : function(){
  		this.getVueDiscounts(this.pagination.current_page);
      this.getProducts();
  },

  methods : {

    getProducts: function() {
      this.$http.get('/list-products').then((response) => {
        this.$set('products', response.data);
      });
    },

    getVueDiscounts: function(page){
      this.$http.get('/discounts?page='+page+'&product='+this.searchProduct).then((response) => {
        this.$set('discounts', response.data.data.data);
        this.$set('pagination', response.data.pagination);
      });
    },

    createDiscount: function(){
		  var input = this.newDiscount;
		  this.$http.post('/discounts',input).then((response) => {
		    this.changePage(this.pagination.current_page);
			   $("#discount-input").focus();
         $("#discount-add-succes").show();
			   toastr.success('Discount Created Successfully.', 'Success Alert', {timeOut: 5000});
		  }, (response) => {
			this.formErrors = response.data;
	    });
	  },

      deleteDiscount: function(discount) {
        var _this = this;
        bootbox.confirm("Are you sure you want to delete!", function(result) { 
          if(result) {
            _this.$http.delete('/discounts/'+discount.id).then((response) => {
              _this.changePage(_this.pagination.current_page);
              toastr.success('Discount Deleted Successfully.', 'Success Alert', {timeOut: 5000});
            });
          }
        });
      },

      editDiscount: function(discount){
          this.fillDiscount.no_of_person = discount.no_of_person;
          this.fillDiscount.discount = discount.discount;
          this.fillDiscount.product_id = discount.product_id;
          this.fillDiscount.payment_cycle = discount.payment_cycle;
          this.fillDiscount.id = discount.id;
          $("#edit-discount").modal('show');
      },

      updateDiscount: function(id){
        var input = this.fillDiscount;
        this.$http.put('/discounts/'+id,input).then((response) => {
            this.changePage(this.pagination.current_page);
            this.fillDiscount = {'no_of_person':'','payment_cycle':'','discount':'','product_id':'', 'id': ''};
            $("#edit-discount").modal('hide');
            toastr.success('Discount Updated Successfully.', 'Success Alert', {timeOut: 5000});
          }, (response) => {
              this.formErrorsUpdate = response.data;
          });
      },

      changePage: function (page) {
          this.pagination.current_page = page;
          this.getVueDiscounts(page);
      }

  }

});