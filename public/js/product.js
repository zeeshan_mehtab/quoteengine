Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({

  el: '#manage-vue',

  data: {
    products: [],
    pagination: {
        total: 0, 
        per_page: 2,
        from: 1, 
        to: 0,
        current_page: 1
      },
    offset: 4,
    formErrors:{},
    formErrorsUpdate:{},
    newProduct : {'name':'','description':''},
    fillProduct : {'name':'','description':'','id':''},
  },

  computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },

  ready : function(){
  		this.getVueProducts(this.pagination.current_page);
  },

  methods : {

        getVueProducts: function(page){
          this.$http.get('/products?page='+page).then((response) => {
            this.$set('products', response.data.data.data);
            this.$set('pagination', response.data.pagination);
          });
        },

    createProduct: function(){
		  var input = this.newProduct;
		  this.$http.post('/products',input).then((response) => {
		    this.changePage(this.pagination.current_page);
			  this.newProduct = {'name':'','description':''};
			  $("#create-product").modal('hide');
			  toastr.success('Product Created Successfully.', 'Success Alert', {timeOut: 5000});
		  }, (response) => {
			this.formErrors = response.data;
	    });
	  },

      deleteProduct: function(product) {
        var _this = this;
        bootbox.confirm("Are you sure you want to delete!", function(result) { 
          if(result) {
            _this.$http.delete('/products/'+product.id).then((response) => {
              _this.changePage(_this.pagination.current_page);
              toastr.success('Product Deleted Successfully.', 'Success Alert', {timeOut: 5000});
            });
          }
        });
      },

      editProduct: function(product){
          this.fillProduct.name = product.name;
          this.fillProduct.id = product.id;
          this.fillProduct.description = product.description;
          $("#edit-product").modal('show');
      },

      updateProduct: function(id){
        var input = this.fillProduct;
        this.$http.put('/products/'+id,input).then((response) => {
            this.changePage(this.pagination.current_page);
            this.fillProduct = {'name':'','description':'','id':''};
            $("#edit-product").modal('hide');
            toastr.success('Product Updated Successfully.', 'Success Alert', {timeOut: 5000});
          }, (response) => {
              this.formErrorsUpdate = response.data;
          });
      },

      changePage: function (page) {
          this.pagination.current_page = page;
          this.getVueProducts(page);
      },
  }
});