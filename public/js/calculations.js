Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({

  el: '#manage-vue',

  data: {
    regions:{},
    products:{},
    region_id:'',
    product_id: '',
    age: '',
    data : {'region_id':'','product_id':'','age':'', 'payment_cycle':''},
    calculation: "Unknow",
    errors: {},
  },

  ready : function(){
  	  this.getRegions();
      this.getProducts();
  },

  methods : {

    getProducts: function() {
      this.$http.get('/list-products').then((response) => {
        this.$set('products', response.data);
      });
    },

    getRegions: function() {
      this.$http.get('/list-regions').then((response) => {
        this.$set('regions', response.data);
      });
    },

    calculatePrice2: function(){
      var input = this.newPrice;
      this.$http.post('/calculate-price',input).then((response) => {
          this.$set('calculation', response.data);
      }, (response) => {
      this.errors = response.data;
      });
    },

    calculatePrice: function() {
      var input = this.data;
      this.$http.get('/calculate-price?ages=' + this.data.ages 
                                  + "&product_id=" + this.data.product_id 
                                  + "&region_id=" + this.data.region_id
                                  + "&payment_cycle=" + this.data.payment_cycle
                                  ).then((response) => {
        this.$set('calculation', response.data.total);
      });
    },
  }

});