Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({

  el: '#manage-vue',

  data: {
    prices: [],
    pagination: {
        total: 0, 
        per_page: 2,
        from: 1, 
        to: 0,
        current_page: 1
      },
    offset: 4,
    formErrors:{},
    formErrorsUpdate:{},
    newPrice : {'age':'','price':'','product_id':'','region_id':''},
    fillPrice : {'age':'','price':'','product_id':'','region_id':'', 'id': ''},
    regions:{},
    products:{},
    selectedProduct:'',
    ageSplit: 1,
    searchProduct: '',
    searchRegion: '',
  },

  computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },

  ready : function(){
  		this.getVuePrices(this.pagination.current_page);
      this.getRegions();
      this.getProducts();
  },

  methods : {

    getProducts: function() {
      this.$http.get('/list-products').then((response) => {
        this.$set('products', response.data);
      });
    },

    getRegions: function() {
      this.$http.get('/list-regions').then((response) => {
        this.$set('regions', response.data);
      });
    },

    getVuePrices: function(page){
      this.$http.get('/prices?page='+page+'&product='+this.searchProduct+'&region='+this.searchRegion).then((response) => {
        this.$set('prices', response.data.data.data);
        this.$set('pagination', response.data.pagination);
      });
    },

    createPrice: function(){
		  var input = this.newPrice;
		  this.$http.post('/prices',input).then((response) => {
		    this.changePage(this.pagination.current_page);
			this.newPrice.age = parseInt(this.newPrice.age) + 1;
      this.newPrice.price = '';
			$("#price-input").focus();
      $("#price-add-succes").show();
			toastr.success('Price Created Successfully.', 'Success Alert', {timeOut: 5000});
		  }, (response) => {
			this.formErrors = response.data;
	    });
	  },

      deletePrice: function(price) {
        var _this = this;
        bootbox.confirm("Are you sure you want to delete!", function(result) { 
          if(result) {
            _this.$http.delete('/prices/'+price.id).then((response) => {
              _this.changePage(_this.pagination.current_page);
              toastr.success('Price Deleted Successfully.', 'Success Alert', {timeOut: 5000});
            });
          }
        });
      },

      editPrice: function(price){
          this.fillPrice.age = price.age;
          this.fillPrice.price = price.price;
          this.fillPrice.product_id = price.product.id;
          this.fillPrice.region_id = price.region_id;
          this.fillPrice.id = price.id;
          $("#edit-price").modal('show');
      },

      updatePrice: function(id){
        var input = this.fillPrice;
        this.$http.put('/prices/'+id,input).then((response) => {
            this.changePage(this.pagination.current_page);
            this.fillPrice = {'age':'','price':'','product_id':'','region_id':'', 'id': ''};
            $("#edit-price").modal('hide');
            toastr.success('Price Updated Successfully.', 'Success Alert', {timeOut: 5000});
          }, (response) => {
              this.formErrorsUpdate = response.data;
          });
      },

      changePage: function (page) {
          this.pagination.current_page = page;
          this.getVuePrices(page);
      }

  }

});