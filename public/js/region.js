Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({

  el: '#manage-vue',

  data: {
    regions: [],
    pagination: {
        total: 0, 
        per_page: 2,
        from: 1, 
        to: 0,
        current_page: 1
      },
    offset: 4,
    formErrors:{},
    formErrorsUpdate:{},
    newRegion : {'name':'','description':''},
    fillRegion : {'name':'','description':'','id':''}
  },

  computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },

  ready : function(){
  		this.getVueRegions(this.pagination.current_page);
  },

  methods : {

        getVueRegions: function(page){
          this.$http.get('/regions?page='+page).then((response) => {
            this.$set('regions', response.data.data.data);
            this.$set('pagination', response.data.pagination);
          });
        },

    createRegion: function(){
		  var input = this.newRegion;
		  this.$http.post('/regions',input).then((response) => {
		    this.changePage(this.pagination.current_page);
			this.newRegion = {'name':'','description':''};
			$("#create-region").modal('hide');
			toastr.success('Region Created Successfully.', 'Success Alert', {timeOut: 5000});
		  }, (response) => {
			this.formErrors = response.data;
	    });
	  },

      deleteRegion: function(region) {
        var _this = this;
        bootbox.confirm("Are you sure you want to delete!", function(result) { 
          if(result) {
            _this.$http.delete('/regions/'+region.id).then((response) => {
              _this.changePage(_this.pagination.current_page);
              toastr.success('Region Deleted Successfully.', 'Success Alert', {timeOut: 5000});
            });
          }
        });
      },

      editRegion: function(region){
          this.fillRegion.name = region.name;
          this.fillRegion.id = region.id;
          this.fillRegion.description = region.description;
          $("#edit-region").modal('show');
      },

      updateRegion: function(id){
        var input = this.fillRegion;
        this.$http.put('/regions/'+id,input).then((response) => {
            this.changePage(this.pagination.current_page);
            this.fillRegion = {'name':'','description':'','id':''};
            $("#edit-region").modal('hide');
            toastr.success('Region Updated Successfully.', 'Success Alert', {timeOut: 5000});
          }, (response) => {
              this.formErrorsUpdate = response.data;
          });
      },

      changePage: function (page) {
          this.pagination.current_page = page;
          this.getVueRegions(page);
      }

  }

});