@extends('admin.layouts.default')

@section('title', 'Dashboard')

{{-- Content --}}
@section('content')
<div class="flex-center position-ref full-height">
    
</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Regions
@stop

{{-- Page Heading --}}
@section('heading')
Regions <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Regions</a>
</li>
@stop
