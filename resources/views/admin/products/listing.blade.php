
<table class="table table-bordered" v-show="products.length">
	<tr>
		<th>Name</th>
		<th>Description</th>
		<th width="100px"></th>
	</tr>
	<tr v-for="product in products">
		<td>@{{ product.name }}</td>
		<td>@{{ product.description }}</td>
		<td >
      <a href="#" @click.prevent="editProduct(product)" title="Edit Product">
        <i class="fa fa-pencil fa-lg fa-fw"></i>
      <a>
      <a href="#" @click.prevent="deleteProduct(product)" title="Delete Product">
        <i class="fa fa-trash fa-lg fa-fw"></i>
      </a>
    </td>
	</tr>
</table>

<div v-else class="alert alert-info" role="alert">
  No products found! Please add produc
  ts by clicking Create Product button.
</div>

<div class="row">
    <div class="col-lg-6">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-product">
		  Create Product
		</button>
    </div>

    <!-- Start Pagination -->
    <div class="col-lg-6">
        <ul class="pagination pull-right no-margin">
            <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous"
                   @click.prevent="changePage(pagination.current_page - 1)">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li v-for="page in pagesNumber"
                v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#"
                   @click.prevent="changePage(page)">@{{ page }}</a>
            </li>
            <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next"
                   @click.prevent="changePage(pagination.current_page + 1)">
                    <span aria-hidden="true">»</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- End Pagination -->
</div>