@extends('admin.layouts.default')

@section('title', 'Products')

{{-- Content --}}

@section('content')

<div id="manage-vue">

	<!-- Region Listing -->
	@include('admin.products.listing')

	<!-- Create Region Modal -->
	@include('admin.products.create-modal')

	<!-- Edit Region Modal -->
	@include('admin.products.edit-modal')

</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Products
@stop

{{-- Page Heading --}}
@section('heading')
Products <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Products</a>
</li>
@stop

@section('page-level-scripts')
<script src="{{ asset('js/product.js') }}"></script>
@stop