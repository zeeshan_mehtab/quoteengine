<div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        		<h4 class="modal-title" id="myModalLabel">Create Product</h4>
      		</div>
      		
      		<div class="modal-body">
      			<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createProduct">
      				<div class="form-group">
						<label for="name">Name:</label>
				<input type="text" name="name" class="form-control" v-model="newProduct.name" />
				<span v-if="formErrors['name']" class="error text-danger">@{{ formErrors['name'] }}</span>
			</div>

			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" v-model="newProduct.description"></textarea>
				<span v-if="formErrors['description']" class="error text-danger">@{{ formErrors['description'] }}</span>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>

      		</form>
      </div>
    </div>
  </div>
</div>