<table class="table table-bordered" v-show="regions.length">
	<tr>
		<th>Name</th>
		<th>Description</th>
		<th width="100px"></th>
	</tr>
	<tr v-for="region in regions">
		<td>@{{ region.name }}</td>
		<td>@{{ region.description }}</td>
		<td>  
          <a href="#" @click.prevent="editRegion(region)" title="Edit Region">
            <i class="fa fa-pencil fa-lg fa-fw"></i>
          <a>
          <a href="#" @click.prevent="deleteRegion(region)" title="Delete Region">
            <i class="fa fa-trash fa-lg fa-fw"></i>
          </a>
        </td>
	</tr>
</table>

<div v-else class="alert alert-info" role="alert">
  No regions found! Please add regions by clicking Create Region button.
</div>

<div class="row">
    <div class="col-lg-6">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-region">
		  Create Region
		</button>
    </div>

    <!-- Start Pagination -->
    <div class="col-lg-6">
        <ul class="pagination pull-right no-margin">
            <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous"
                   @click.prevent="changePage(pagination.current_page - 1)">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li v-for="page in pagesNumber"
                v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#"
                   @click.prevent="changePage(page)">@{{ page }}</a>
            </li>
            <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next"
                   @click.prevent="changePage(pagination.current_page + 1)">
                    <span aria-hidden="true">»</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- End Pagination -->
</div>