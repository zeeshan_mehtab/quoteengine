@extends('admin.layouts.default')

@section('title', 'Regions')

{{-- Content --}}
@section('content')

<div id="manage-vue">

	<!-- Region Listing -->
	@include('admin.regions.listing')

	<!-- Create Region Modal -->
	@include('admin.regions.create-modal')

	<!-- Edit Region Modal -->
	@include('admin.regions.edit-modal')	

</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Regions
@stop

{{-- Page Heading --}}
@section('heading')
Regions <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Regions</a>
</li>
@stop

@section('page-level-scripts')
<script src="{{ asset('js/region.js') }}"></script>
@stop