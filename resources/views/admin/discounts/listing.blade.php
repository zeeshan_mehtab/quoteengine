<div class="row">
  <div class="form-group col-sm-6">
    <select name="search_product" class="form-control input-sm" v-model="searchProduct" v-on:change="getVueDiscounts(1)">
        <option value="">Select Product</option>
        <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
    </select>
  </div>
</div>
<div class="row" v-show="discounts.length">
  <div class="col-sm-12">
    <table class="table table-bordered">
    	<tr>
    		<th>Product</th>
    		<th>Min. No of Person</th>
        <th>Payment Cycle</th>
        <th>Discount</th>
    		<th width="100px"></th>
    	</tr>
    	<tr v-for="discount in discounts">
    		<td>@{{ discount.product.name }}</td>
        <td>@{{ discount.no_of_person }}</td>
        <td>
            <span v-if="discount.payment_cycle == 0">Any</span>
            <span v-if="discount.payment_cycle == 1">Monthly</span>
            <span v-if="discount.payment_cycle == 3">Quaterly</span>
            <span v-if="discount.payment_cycle == 6">Bi Annually</span>
            <span v-if="discount.payment_cycle == 12">Annually</span>
        </td>
        <td>@{{ discount.discount }}%</td>
    		<td>  
              <a href="#" @click.prevent="editDiscount(discount)" title="Edit Discount">
                <i class="fa fa-pencil fa-lg fa-fw"></i>
              <a>
              <a href="#" @click.prevent="deleteDiscount(discount)" title="Delete Discount">
                <i class="fa fa-trash fa-lg fa-fw"></i>
              </a>
            </td>
    	</tr>
    </table>
  </div>
</div>
<div v-else class="alert alert-info" role="alert">
  No discounts found! Please add discounts by clicking Create Discount button.
</div>

<div class="row">
    <div class="col-lg-6">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-discount">
		  Create Discount
		</button>
    </div>

    <!-- Start Pagination -->
    <div class="col-lg-6">
        <ul class="pagination pull-right no-margin">
            <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous"
                   @click.prevent="changePage(pagination.current_page - 1)">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li v-for="page in pagesNumber"
                v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#"
                   @click.prevent="changePage(page)">@{{ page }}</a>
            </li>
            <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next"
                   @click.prevent="changePage(pagination.current_page + 1)">
                    <span aria-hidden="true">»</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- End Pagination -->
</div>