<div class="modal fade" id="edit-discount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Discount</h4>
      </div>
      <div class="modal-body">

      	<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateDiscount(fillDiscount.id)">

          <div class="form-group col-lg-6 col-md-6 col-sm-12">
                  <label for="product_id">Product*:</label>
                  <select name="product_id" class="form-control" v-model="fillDiscount.product_id">
                <option value="">Select Product</option>
                <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
            </select>
                  <span v-if="formErrorsUpdate['product_id']" class="error text-danger">@{{ formErrorsUpdate['product_id'] }}</span>
              </div>

              <div class="form-group col-lg-6 col-md-6 col-sm-12">
              <label for="no_of_person">Min. No of Person*:</label>
              <input type="number" name="no_of_person" class="form-control" v-model="fillDiscount.no_of_person" min="0" max="150"/>
              <span v-if="formErrorsUpdate['no_of_person']" class="error text-danger">@{{ formErrorsUpdate['no_of_person'] }}</span>
            </div>

            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="payment_cycle">Payment Cycle*:</label>
                <select name="payment_cycle" class="form-control" v-model="fillDiscount.payment_cycle">
                <option value="0">Any (Does not matter)</option>
                <option value="1">Monthly</option>
                <option value="3">Quaterly</option>
                <option value="6">Bi Annualy</option>
                <option value="12">Annualy</option>
            </select>
            <span v-if="formErrorsUpdate['payment_cycle']" class="error text-danger">@{{ formErrorsUpdate['payment_cycle'] }}</span>
            </div>

                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                  <label for="discount">Discount*:</label>
                  <div class="input-group">
                    <input type="number" name="discount" id="discount-input" class="form-control" v-model="fillDiscount.discount" />
                    <span class="input-group-addon">%</span>
                  </div>
                  <span v-if="formErrorsUpdate['discount']" class="error text-danger">@{{ formErrorsUpdate['discount'] }}</span>
                </div>

          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>

      		</form>

      </div>
    </div>
  </div>
</div>