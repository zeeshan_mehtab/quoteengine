<div class="modal fade" id="create-discount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        		<h4 class="modal-title" id="myModalLabel">Create Discount</h4>
      		</div>
      		
      		<div class="modal-body">
      			<div class="alert alert-success" role="alert" style="display: none;" id="discount-add-succes">
				 	<strong>Success!</strong> Discount added. Please continue adding next discount.
				</div>
      			<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createDiscount">

      				<div class="form-group col-lg-6 col-md-6 col-sm-12">
			            <label for="product_id">Product*:</label>
			            <select name="product_id" class="form-control" v-model="newDiscount.product_id">
						    <option value="">Select Product</option>
						    <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
						</select>
			            <span v-if="formErrors['product_id']" class="error text-danger">@{{ formErrors['product_id'] }}</span>
			        </div>

      				<div class="form-group col-lg-6 col-md-6 col-sm-12">
	    				<label for="no_of_person">Min. No of Person*:</label>
	    				<input type="number" name="no_of_person" class="form-control" v-model="newDiscount.no_of_person" min="0" max="150"/>
	    				<span v-if="formErrors['no_of_person']" class="error text-danger">@{{ formErrors['no_of_person'] }}</span>
	    			</div>

	    			<div class="form-group col-lg-6 col-md-6 col-sm-12">
				        <label for="payment_cycle">Payment Cycle*:</label>
				        <select name="payment_cycle" class="form-control" v-model="newDiscount.payment_cycle" required>
						    <option value="">Select Payment Cycle</option>
						    <option value="0">Any (Does not matter)</option>
						    <option value="1">Monthly</option>
						    <option value="3">Quaterly</option>
						    <option value="6">Bi Annualy</option>
						    <option value="12">Annualy</option>
						</select>
						<span v-if="formErrors['payment_cycle']" class="error text-danger">@{{ formErrors['payment_cycle'] }}</span>
				    </div>

			          <div class="form-group col-lg-6 col-md-6 col-sm-12">
			            <label for="discount">Discount*:</label>
			            <div class="input-group">
			            	<input type="number" name="discount" id="discount-input" class="form-control" v-model="newDiscount.discount" />
			            	<span class="input-group-addon">%</span>
			            </div>
			            <span v-if="formErrors['discount']" class="error text-danger">@{{ formErrors['discount'] }}</span>
			          </div>Min. 

					<div class="form-group">
						<button type="submit" class="btn btn-success margin-left-15">Submit</button>
					</div>
      		</form>
      </div>
    </div>
  </div>
</div>