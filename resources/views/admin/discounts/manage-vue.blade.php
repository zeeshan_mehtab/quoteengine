@extends('admin.layouts.default')

@section('title', 'Discounts')

{{-- Content --}}
@section('content')

<div id="manage-vue">

	<!-- Discount Listing -->
	@include('admin.discounts.listing')

	<!-- Create Discount Modal -->
	@include('admin.discounts.create-modal')

	<!-- Edit Discount Modal -->
	@include('admin.discounts.edit-modal')	

</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Discounts
@stop

{{-- Page Heading --}}
@section('heading')
Discounts <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Discounts</a>
</li>
@stop

@section('page-level-scripts')
<script src="{{ asset('js/discount.js') }}"></script>
@stop