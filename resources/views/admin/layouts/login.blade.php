<!DOCTYPE html>

<html lang="en">

    <!-- BEGIN HEAD -->
    @include('admin.layouts.partials.head')
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body>
        
        <!-- BEGIN CONTAINER -->

        <div class="container">
            @yield('content')
        </div>

        <!-- END CONTAINER -->
        
        @include('admin.layouts.partials.footer')
        
    </body>
</html>
