<!DOCTYPE html>

<html lang="en">

    <!-- BEGIN HEAD -->
    @include('admin.partials.head')
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body>
        
        <!-- BEGIN CONTAINER -->

        <div id="wrapper">

            <!-- Navigation -->
            @include('admin.partials.navigation')    

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('title')</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>    
                
                @yield('content')
                
            </div>
                
        </div>

        <!-- END CONTAINER -->
        
        @include('admin.partials.footer')
        
        @yield('page-level-scripts')
    </body>
</html>
