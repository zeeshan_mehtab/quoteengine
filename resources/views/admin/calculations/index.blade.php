@extends('admin.layouts.default')

@section('title', 'Price Calculator')

{{-- Content --}}
@section('content')

<div id="manage-vue">

	<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="calculatePrice">
		<div class="row">
			<div class="form-group col-lg-6 col-md-6 col-sm-12">
		        <label for="product_id">Product*:</label>
		        <select name="product_id" class="form-control" v-model="data.product_id" required>
				    <option value="">Select Product</option>
				    <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
				</select>
		    </div>

		    <div class="form-group col-lg-6 col-md-6 col-sm-12">
		        <label for="region_id">Region*:</label>
		        <select name="region_id" class="form-control" v-model="data.region_id" required>
				    <option value="" disabled>Select Region</option>
				    <option v-for="region in regions" v-bind:value="region.id">@{{ region.name }}</option>
				</select>
		    </div>

			<div class="form-group col-lg-6 col-md-6 col-sm-12">
				<label for="age">Ages(Comma Seperated)*:</label>
				<input type="string" name="ages" class="form-control" v-model="data.ages" required/>
			</div>

			<div class="form-group col-lg-6 col-md-6 col-sm-12">
		        <label for="payment_cycle">Payment Cycle*:</label>
		        <select name="payment_cycle" class="form-control" v-model="data.payment_cycle" required>
				    <option value="">Select Payment Cycle</option>
				    <option value="1">Monthly</option>
				    <option value="3">Quaterly</option>
				    <option value="6">Bi Annualy</option>
				    <option value="12">Annualy</option>
				</select>
		    </div>
		</div>

		<div class="form-group row">
			<button type="submit" class="btn btn-success margin-left-15">Submit</button>
		</div>

		<div class="alert alert-success" role="alert">
		 	<strong>Price:</strong> Based on above input the total price will be <strong>@{{ calculation }}</storng>.
		</div>
	</form>
</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Prices
@stop

{{-- Page Heading --}}
@section('heading')
Prices <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Prices</a>
</li>
@stop

@section('page-level-scripts')
<script src="{{ asset('js/calculations.js') }}"></script>
@stop