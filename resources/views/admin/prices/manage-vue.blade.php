@extends('admin.layouts.default')

@section('title', 'Prices')

{{-- Content --}}
@section('content')

<div id="manage-vue">

	<!-- Price Listing -->
	@include('admin.prices.listing')

	<!-- Create Price Modal -->
	@include('admin.prices.create-modal')

	<!-- Edit Price Modal -->
	@include('admin.prices.edit-modal')	

</div>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Prices
@stop

{{-- Page Heading --}}
@section('heading')
Prices <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Prices</a>
</li>
@stop

@section('page-level-scripts')
<script src="{{ asset('js/price.js') }}"></script>
@stop