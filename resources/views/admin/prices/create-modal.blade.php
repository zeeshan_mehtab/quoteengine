<div class="modal fade" id="create-price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        		<h4 class="modal-title" id="myModalLabel">Create Price</h4>
      		</div>
      		
      		<div class="modal-body">
      			<div class="alert alert-success" role="alert" style="display: none;" id="price-add-succes">
				 	<strong>Success!</strong> Price added. Please continue adding next price.
				</div>	
      			<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createPrice">

      				<div class="form-group col-lg-6 col-md-6 col-sm-12">
			            <label for="product_id">Product*:</label>
			            <select name="product_id" class="form-control" v-model="newPrice.product_id">
						    <option value="">Select Product</option>
						    <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
						</select>
			            <span v-if="formErrors['product_id']" class="error text-danger">@{{ formErrors['product_id'] }}</span>
			          </div>

			          <div class="form-group col-lg-6 col-md-6 col-sm-12">
			            <label for="region_id">Region*:</label>
			            <select name="region_id" class="form-control" v-model="newPrice.region_id">
						    <option value="" disabled>Select Region</option>
						    <option v-for="region in regions" v-bind:value="region.id">@{{ region.name }}</option>
						</select>
			            <span v-if="formErrors['region_id']" class="error text-danger">@{{ formErrors['region_id'] }}</span>
			          </div>

      				<div class="form-group col-lg-6 col-md-6 col-sm-12">
	    				<label for="age">Age*:</label>
	    				<div class="input-group">
	    					<input type="number" name="age" class="form-control" v-model="newPrice.age" min="0" max="150"/>
	    					<span class="input-group-addon">Years</span>
	    				</div>
	    				<span v-if="formErrors['age']" class="error text-danger">@{{ formErrors['age'] }}</span>
	    			</div>

			          <div class="form-group col-lg-6 col-md-6 col-sm-12">
			            <label for="price">Price*:</label>
			            <div class="input-group">
			            	<span class="input-group-addon">&euro;</span>
			            	<input type="number" name="price" id="price-input" class="form-control" v-model="newPrice.price" />
			            </div>
			            <span v-if="formErrors['price']" class="error text-danger">@{{ formErrors['price'] }}</span>
			          </div>

					<div class="form-group">
						<button type="submit" class="btn btn-success margin-left-15">Submit</button>
					</div>
      		</form>
      </div>
    </div>
  </div>
</div>