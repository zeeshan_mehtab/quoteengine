<div class="row">
    <div class="col-lg-6">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-price">
            Create Price
        </button>
    </div>

    <!-- Start Pagination -->
    <div class="col-lg-6">
        <ul class="pagination pull-right no-margin">
            <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous"
                   @click.prevent="changePage(pagination.current_page - 1)">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li v-for="page in pagesNumber"
                v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#"
                   @click.prevent="changePage(page)">@{{ page }}</a>
            </li>
            <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next"
                   @click.prevent="changePage(pagination.current_page + 1)">
                    <span aria-hidden="true">»</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- End Pagination -->
</div>
<hr/>
<div class="row">
  <div class="form-group col-sm-6">
    <select name="search_product" class="form-control input-sm" v-model="searchProduct" v-on:change="getVuePrices(1)">
        <option value="">Select Product</option>
        <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
    </select>
  </div>
  <div class="form-group col-sm-6">
      <select name="search_region" class="form-control input-sm" v-model="searchRegion" v-on:change="getVuePrices(1)">
        <option value="">Select Region</option>
        <option v-for="region in regions" v-bind:value="region.id">@{{ region.name }}</option>
    </select>
  </div>
</div>

<div class="row" v-show="prices.length">
  <div class="col-sm-12">
    <table class="table table-bordered">
    	<tr>
            <th>Product</th>
            <th>Region</th>
            <th>Age</th>
    		<th>Price</th>
    		<th width="100px"></th>
    	</tr>
    	<tr v-for="price in prices">
            <td>@{{ price.product.name }}</td>
            <td><span v-if="price.region.name">@{{ price.region.name }}</span><span v-else>Resto</span></td>
    		<td>@{{ price.age }} Years</td>
            <td>&euro; @{{ price.price }}</td>
    		<td>  
              <a href="#" @click.prevent="editPrice(price)" title="Edit Price">
                <i class="fa fa-pencil fa-lg fa-fw"></i>
              <a>
              <a href="#" @click.prevent="deletePrice(price)" title="Delete Price">
                <i class="fa fa-trash fa-lg fa-fw"></i>
              </a>
            </td>
    	</tr>
    </table>
  </div>
</div>
<div v-else class="alert alert-info" role="alert">
  No prices found! Please add prices by clicking Create Price button.
</div>