<div class="modal fade" id="edit-price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Price</h4>
      </div>
      <div class="modal-body">

      	<form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updatePrice(fillPrice.id)">

          <div class="form-group col-lg-6 col-md-6 col-sm-12">
            <label for="product_id">Product*:</label>
            <select name="product_id" class="form-control" v-model="fillPrice.product_id">
                <option value="">Select Product</option>
                <option v-for="product in products" v-bind:value="product.id">@{{ product.name }}</option>
            </select>
            <span v-if="formErrorsUpdate['product_id']" class="error text-danger">@{{ formErrorsUpdate['product_id'] }}</span>
          </div>

          <div class="form-group col-lg-6 col-md-6 col-sm-12">
            <label for="region_id">Region*:</label>
            <select name="region_id" class="form-control" v-model="fillPrice.region_id">
                <option value="" disabled>Select Region</option>
                <option v-for="region in regions" v-bind:value="region.id">@{{ region.name }}</option>
            </select>
            <span v-if="formErrorsUpdate['region_id']" class="error text-danger">@{{ formErrorsUpdate['region_id'] }}</span>
          </div>

          <div class="form-group col-lg-6 col-md-6 col-sm-12">
              <label for="age">Age*:</label>
              <div class="input-group">
                <input type="number" name="age" class="form-control" v-model="fillPrice.age" min="0" max="150"/>
                <span class="input-group-addon">Years</span>
              </div>
              <span v-if="formErrorsUpdate['age']" class="error text-danger">@{{ formErrorsUpdate['from_age'] }}</span>
            </div>

            <div class="form-group col-lg-6 col-md-6 col-sm-12">
              <label for="price">Price*:</label>
              <div class="input-group">
                <span class="input-group-addon">&euro;</span>
                <input type="number" name="price" id="price-input" class="form-control" v-model="fillPrice.price" />
              </div>
              <span v-if="formErrorsUpdate['price']" class="error text-danger">@{{ formErrorsUpdate['price'] }}</span>
            </div>

          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>

      		</form>

      </div>
    </div>
  </div>
</div>