@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="POST" enctype="multipart/form-data" action="/price-import">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <label for="price">File*:</label>
                    <input type="file" name="file" class="form-control" required/>
                    <span class="error text-danger">Allowed file format: xls</span>
                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-success" style="margin-top:25px;">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form method="POST" enctype="multipart/form-data" action="/price-remove-all">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group ">
                    {{--<button onclick="javascript: return bootbox.confirm('Are you sure you want to remove all previous prices?')" type="submit" class="btn btn-danger">Remove All Existing Prices</button>--}}
                </div>
            </form>
        </div>
    </div>
@stop

{{-- Web site Title --}}
@section('title')
    @parent
    Import Prices
@stop

{{-- Page Heading --}}
@section('heading')
    Import Vehicles
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
    <li>
        <a href="{{ action('PricesController@manage') }}">Prices</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="#">Import Prices</a>
    </li>
@stop
