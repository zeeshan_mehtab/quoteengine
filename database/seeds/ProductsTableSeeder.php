<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Plan;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
	        $product = new Product();
	        $product->name = 'Basic Plan';
	        $product->description = 'Basic Plan';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Classic Plan';
	        $product->description = 'Classic Plan';
	        $product->save();     	
        }

        {
	        $product = new Product();
	        $product->name = 'Students Plan';
	        $product->description = 'Students Plan';
	        $product->save();     	
        }

        {
	        $product = new Product();
	        $product->name = 'Mas salud optima with';
	        $product->description = 'Mas salud optima (with co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Mas salud optima without)';
	        $product->description = 'Mas salud optima (without co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Mas 90 plus with';
	        $product->description = 'Mas 90 plus (with co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Mas 90 plus without';
	        $product->description = 'Mas 90 plus (without co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Premium 500';
	        $product->description = 'Premium 500';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Complete plan';
	        $product->description = 'Complete plan';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Mundi 1 million';
	        $product->description = 'Mundi 1 million';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Professional plus with';
	        $product->description = 'Professional plus (with co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Professional plus without';
	        $product->description = 'Professional plus (without co-payments)';
	        $product->save();
        }

        {
	        $product = new Product();
	        $product->name = 'Primero';
	        $product->description = 'Primero';
	        $product->save();
        }
       
    }
}
