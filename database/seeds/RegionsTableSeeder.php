<?php

use Illuminate\Database\Seeder;
use App\Models\Region;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            "A Coruña",
            "Alava",
            "Albacete",
            "Alicante",
            "Almeria",
            "Asturias",
            "Avila",
            "Badajoz",
            "Mallorca",
            "Formentera",
            "Barcelona",
            "Burgos",
            "Caceres",
            "Cadiz",
            "Cantabria",
            "Castellon",
            "Ceuta",
            "Cuidad Real",
            "Cordoba",
            "Cuenca",
            "Girona",
            "Granada",
            "Guadalajara",
            "Guipozcoa",
            "Huelva",
            "Huesca",
            "Ibiza",
            "Jaen",
            "La Rioja",
            "Las Palmas",
            "Leon",
            "Lleida",
            "Lugo",
            "Madrid",
            "Malaga",
            "Melilla",
            "Menorc",
            "Murcia",
            "Navarra",
            "Ourense",
            "Palencia",
            "Pontevedra",
            "Salamanca",
            "Segovia",
            "Sevilla",
            "Soria",
            "Tarragona",
            "Tenerife",
            "Teruel",
            "Toledo",
            "Valencia",
            "Valladolid",
            "Vizcaya",
            "Zamora",
            "Zaragoza",
        ];

        foreach ($regions as $region) {
            DB::table('regions')->insert(['name' => $region, 'description' => $region]);
        }

    }
}
